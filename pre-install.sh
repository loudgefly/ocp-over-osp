#! /bin/bash
SSH_KEY_PATH='~/.ssh/id_rsa'
OCP_INSTALLER_DOWNLOAD_URL='https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux.tar.gz'
OCP_CLIENT_DOWNLOAD_URL='https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux.tar.gz'

echo -e "Loading OCP tenant RC"
source ocp-tenant-openrc.sh

echo -e "Creating master flavor"
openstack flavor create --ram 16384 --vcpu 4 --disk 25 master

echo -e "Setting Object Store temp key"
openstack object store account set --property Temp-URL-Key=superkey

echo -e "Dowloading and extracting OCP installer"
wget $OCP_INSTALLER_DOWNLOAD_URL
tar -xvzf openshift-install-linux.tar.gz -C .

echo -e "Dowloading and extracting OCP client"
wget $OCP_CLIENT_DOWNLOAD_URL
tar -xvzf openshift-client-linux.tar.gz -C .

echo -e "Adding SSH key to ssh agent"
eval "$(ssh-agent -s)"
ssh-add $SSH_KEY_PATH

echo -e "Configure KUBECONFIG path"
export KUBECONFIG='./auth/kubeconfig'
