terraform {
  required_providers {

    openstack = {
      source  = "terraform-providers/openstack"
      version = "1.31.0"
    }

  }
}

# Configure the OpenStack Provider
provider "openstack" {
  user_name   = var.os_user
  tenant_name = var.os_tenant
  password    = var.os_password
  auth_url    = var.os_auth_url
  region      = var.os_region
}

data "openstack_identity_user_v3" "admin_user" {
  name = "admin"
}

data "openstack_identity_role_v3" "admin_role" {
  name = "admin"
}

data "openstack_identity_role_v3" "member_role" {
  name = "_member_"
}

data "openstack_networking_network_v2" "external_network" {
  network_id = var.external_network_id
}

resource "openstack_identity_project_v3" "ocp_project" {
  name        = var.ocp_project.name
  description = var.ocp_project.desc
}

resource "openstack_identity_user_v3" "ocp_user" {
  default_project_id = openstack_identity_project_v3.ocp_project.id
  name               = var.ocp_user.name
  description        = var.ocp_user.desc

  password = var.ocp_user.pwd

  ignore_change_password_upon_first_use = true

  extra = {
    email = var.ocp_user.email
  }
}

resource "openstack_identity_role_assignment_v3" "ocp_role_assignment_1" {
  user_id    = openstack_identity_user_v3.ocp_user.id
  project_id = openstack_identity_project_v3.ocp_project.id
  role_id    = data.openstack_identity_role_v3.member_role.id
}

resource "openstack_identity_role_assignment_v3" "ocp_role_assignment_2" {
  user_id    = openstack_identity_user_v3.ocp_user.id
  project_id = openstack_identity_project_v3.ocp_project.id
  role_id    = data.openstack_identity_role_v3.admin_role.id
}

resource "openstack_identity_role_assignment_v3" "ocp_role_assignment_3" {
  user_id    = data.openstack_identity_user_v3.admin_user.id
  project_id = openstack_identity_project_v3.ocp_project.id
  role_id    = data.openstack_identity_role_v3.admin_role.id
}

#resource "openstack_identity_role_assignment_v3" "ocp_role_assignment_4" {
#  user_id    = openstack_identity_user_v3.ocp_user.id
#  project_id = openstack_identity_project_v3.ocp_project.id
#  role_id    = data.openstack_identity_role_v3.swiftoperator_role.id
#}

resource "openstack_compute_quotaset_v2" "ocp_compute_quota" {
  project_id                  = openstack_identity_project_v3.ocp_project.id
  instances                   = var.ocp_tenant_quotas.compute.instances
  cores                       = var.ocp_tenant_quotas.compute.cores
  ram                         = var.ocp_tenant_quotas.compute.ram
  metadata_items              = var.ocp_tenant_quotas.compute.metadata_items
  key_pairs                   = var.ocp_tenant_quotas.compute.key_pairs
  server_groups               = var.ocp_tenant_quotas.compute.server_groups
  server_group_members        = var.ocp_tenant_quotas.compute.server_group_members
  injected_files              = var.ocp_tenant_quotas.compute.injected_files
  injected_file_content_bytes = var.ocp_tenant_quotas.compute.injected_file_content_bytes
  injected_file_path_bytes    = var.ocp_tenant_quotas.compute.injected_file_path_bytes
}

resource "openstack_networking_quota_v2" "ocp_net_quota" {
  project_id          = openstack_identity_project_v3.ocp_project.id
  network             = var.ocp_tenant_quotas.network.network
  subnet              = var.ocp_tenant_quotas.network.subnet
  port                = var.ocp_tenant_quotas.network.port
  router              = var.ocp_tenant_quotas.network.router
  floatingip          = var.ocp_tenant_quotas.network.floatingip
  security_group      = var.ocp_tenant_quotas.network.security_group
  security_group_rule = var.ocp_tenant_quotas.network.security_group_rule
}

resource "openstack_networking_floatingip_v2" "app_floatip" {
  pool = data.openstack_networking_network_v2.external_network.name
  tenant_id 	      = openstack_identity_project_v3.ocp_project.id
}

resource "openstack_networking_floatingip_v2" "api_floatip" {
  pool = data.openstack_networking_network_v2.external_network.name
  tenant_id 	      = openstack_identity_project_v3.ocp_project.id
}

resource "local_file" "ocp_tenant_openrc_sh" {
  content = templatefile("ocp-tenant-openrc.sh.j2",
    {
      auth_url     = var.os_auth_url
      username     = openstack_identity_user_v3.ocp_user.name,
      password     = openstack_identity_user_v3.ocp_user.password,
      project_id   = openstack_identity_project_v3.ocp_project.id
      project_name = openstack_identity_project_v3.ocp_project.name
      region       = var.os_region
    }
  )

  filename = "ocp-tenant-openrc.sh"
}

resource "local_file" "clouds_yaml" {
  content = templatefile("clouds.yaml.j2",
    {
      auth_url     = var.os_auth_url
      username     = openstack_identity_user_v3.ocp_user.name,
      password     = openstack_identity_user_v3.ocp_user.password,
      project_id   = openstack_identity_project_v3.ocp_project.id
      project_name = openstack_identity_project_v3.ocp_project.name
      region       = var.os_region
    }
  )

  filename = "clouds.yaml"
}
