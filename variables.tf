variable "os_user" {
  type        = string
  description = "OS auth username"
  default     = "username"
}

variable "os_tenant" {
  type        = string
  description = "OS auth tenant"
  default     = "username"
}

variable "os_password" {
  type        = string
  description = "OS auth password"
  default     = "password"
}

variable "os_auth_url" {
  type        = string
  description = "OS auth URL"
  default     = "http://192.168.0.1:5000/v3/"
}

variable "os_region" {
  type        = string
  description = "OS region"
  default     = "RegionOne"
}

variable "ocp_project" {
  type        = map
  description = "OCP project details"
  default = {
    name = "ocp-tenant",
    desc = "OCP Tenant"
  }
}

variable "ocp_user" {
  type        = map
  description = "OCP user details"
  default = {
    name = "ocp-user"
    pwd  = "openshift"
    desc = "OCP User"
  }
}

variable "ocp_tenant_quotas" {
  type        = map
  description = "OCP tenant quotas"
  default = {
    compute = {
      instances                   = "10"
      cores                       = "40"
      ram                         = "102400"
      metadata_items              = "128"
      key_pairs                   = "100"
      server_groups               = "10"
      server_group_members        = "10"
      injected_files              = "5"
      injected_file_content_bytes = "10240"
      injected_file_path_bytes    = "255"
    }
    network = {
      network             = "100"
      subnet              = "100"
      port                = "500"
      router              = "10"
      floatingip          = "50"
      security_group      = "40",
      security_group_rule = "500"
    }
  }
}

variable "external_network_id" {
  type        = string
  description = "ID of the external network"
}

output "APP_LB_IP" {
  value = openstack_networking_floatingip_v2.app_floatip.address
}

output "API_LB_IP" {
  value = openstack_networking_floatingip_v2.api_floatip.address
}

