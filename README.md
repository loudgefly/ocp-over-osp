# OCP over OSP

The goal of this repo is to collect some resources to automate the installation of OCP 4.6 on an OpenStack cluster.

## Prerequisites

The first step is to install Terraform to run the HCL file contained in this repo. Please find further docs at [Terraform installation page](https://learn.hashicorp.com/tutorials/terraform/install-cli)

To address all future requests to OCP app calls, a couple of IPs and related DNS records are needed. Supposing an `example.org` as the base domain, and `ocp4` as cluster name, the following DNS records would be necessary:

    # zone example.org
    api.ocp4      IN       A     <API-ip> 
    *.apps.ocp4   IN       A     <APPS-ip>

These records should be available while the OCP installation process takes place.

## OpenStack setup

The Openstack environment can be configured using Terraform, in some steps:

1. Create a `terraform.tfvars` file with the content:
```
    os_user     = "<user>"                     # privileges user (easy to find in rc file)
    os_tenant   = "<tenant_name>"              # privileged user tenant (easy to find in rc file) 
    os_password = "<password>"                 # privileged user password (easy to find in rc file)
    os_auth_url = "<auth_URL>"                 # keystone auth URL (easy to find in rc file)
    os_region   = "<region_name>"              # tenant region (easy to find in rc file)
    external_network_id = "<ext_net_UUID>"     # UUID of external network (floating IP network)
    ocp_project = {
      name = "<ocp_tenant_name>",              # OCP tenant name (to be created)
      desc = "<ocp_tenant_description>"        # OCP tenant description (to be created)
    }
    ocp_user = {
      name  = "<ocp_user_username>"            # OCP username (to be created)
      pwd   = "<ocp_user_password>"            # OCP user password (to be created)
      desc  = "<ocp_user_description>"         # OCP user description (to be created)
      email = "<ocp_user_email>"               # OCP user email (to be created)
    }
```
The external network should be choosen wisely and should have almost 2 available IPs.

2. run `terraform init`
3. run `terraform plan` and accurately review all resources 
4. run `terraform apply`
5. read Terraform outputs at the end of the run: those are the IPs to be used to populate previously mentioned DNS records and will be used in next steps. If you need to read them again simply run `terraform output`

The local dir now includes some additional files: `ocp-tenant-openrc.sh` and `clouds.yaml`. They will be needed by OCP installer.
The OCP installer uses an embedded Terraform version, so the installer should be ran **OUTSIDE** the scope of any existing Terraform initialized env, otherwise some version mismatch error will occur. In the following we'll use the sibling dir  `ocp-install` as an example: 

    mkdir ../ocp-install
    cp {ocp-tenant-openrc.sh,clouds.yaml,pre-install.sh} ../ocp-install
    cd ../ocp-install

Now we need to run a short bash script to execute some pre-install task:

    bash pre-install.sh

It will also install the OCP installer and the CLI OCP client. The executable files will be extracted in the current dir.

## Installation
Once our environment is ready to deploy, we need to run: 

    ./openshift-install create install-config --dir=ocp-install

It creates a new directory containing everything needed for the OCP installation, it will ask some question about the new cluster (like the APIs IP we already mentioned), and stores this informations in a config file.
A pull key is also asked for, obtain yours at [OCP installation resources page](https://cloud.redhat.com/openshift/install/openstack/installer-provisioned)
Installation properties can be changed manually in `ocp_install/install-config.yaml`, e.g. the following will disable Octavia and defines used region name: 

    platform:
      openstack:
        ...
        trunkSupport: false
        octaviaSupport: false
        region: RegionOne
        ...

Now everything is ready for the deploy (it will take approx. 30 mins). 

    ./openshift-install --log-level=debug create cluster --dir=ocp-install

Operations will end with something similar: 

    INFO Install complete!                            
    INFO To access the cluster as the system:admin user when using 'oc', run 'export KUBECONFIG=<ocp_install_dir>/auth/kubeconfig' 
    INFO Access the OpenShift web-console here: https://console-openshift-console.apps.<cluster_name>.<your-domain>
    INFO Login to the console with user: "kubeadmin", and password: "****" 

Keep note of the information, one further manual step is needed at this point: we need to bind the apps floating ip created previously to the OCP cluster ingress port.
Source the OCP tenant rc file and check the available floating IP entries:

    # source ocp-tenant-openrc.sh
    # openstack floating ip list       
    +-----------------+---------------------+------------------+--------+-------------------------+--------------------+
    | ID              | Floating IP Address | Fixed IP Address | Port   | Floating Network        | Project            |
    +-----------------+---------------------+------------------+--------+-------------------------+--------------------+
    | <APPS_IP_UUID>  |      <APPS_IP>      | None             | None   | <external_network_UUID> | <ocp_project_UUID> |
    |       ...       |         ...         |       ...        |   ...  |             ...         |        ...         |
    +-----------------+---------------------+------------------+--------+-------------------------+--------------------+

Find the row containig the APPS floating ip (it should have no port assigned) and take note of the ID.
Now run:  

    # openstack port list                                                                             
    +-------------+-------------------------------------------+-------------------+---------------------+--------+
    | ID          | Name                                      | MAC Address       | Fixed IP Addresses  | Status |
    +-------------+-------------------------------------------+-------------------+---------------------+--------+
    | <port_UUID> | <cluster_name>-<cluster_ID>-ingress-port  | AA:BB:CC:DD:EE:FF | ip_address='   ...  | DOWN   |
    |     ...     |                                           |        ...        |         ...         |  ...   |
    +-------------+-------------------------------------------+-------------------+---------------------+--------+

And take note of the UUID of the port named `<cluster_name>-<cluster_ID>-ingress-port`

Finally run: 

    # openstack floating ip set --port <cluster_name>-<cluster_ID>-ingress-port <APPS_IP_UUID>

Now the floating IP should be bound to the OCP ingress port.

**NOTE: you will not be able to reach anything on the OCP cluster until you set up DNS resolution properly**

In case of emergency of for a POC you may insert some (at least) entries in your hosts file:

```
   <API-ip>  api.<cluster_name>.<base_domain>
   <APPS-ip> console-openshift-console.apps.<cluster_name>.<base_domain>
   <APPS-ip> integrated-oauth-server-openshift-authentication.apps.<cluster_name>.<base_domain>
   <APPS-ip> oauth-openshift.apps.<cluster_name>.<base_domain>
   <APPS-ip> prometheus-k8s-openshift-monitoring.apps.<cluster_name>.<base_domain>
   <APPS-ip> grafana-openshift-monitoring.apps.<cluster_name>.<base_domain>
```
